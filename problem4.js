// Q4 Find all users with masters Degree.

function mastersUsers(dataset) {
  return Object.keys(dataset).filter((name) => {
    return dataset[name].qualification == "Masters";
  });
}

module.exports = mastersUsers;
