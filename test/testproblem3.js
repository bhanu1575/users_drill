const dataset = require("../1-users.cjs");
const { sortByDesignation, sortByAge } = require("../problem3.js");

try {
  console.log(sortByDesignation(dataset));
  console.log(sortByAge(dataset));
} catch (error) {
  console.log(error);
}
