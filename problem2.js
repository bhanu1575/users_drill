// Q2 Find all users staying in Germany.
const dataset = require('./1-users.cjs');


function userbyCoutries(dataset,country){
    return Object.keys(dataset).filter((names)=>{
        return dataset[names].nationality == country;
    })
};

module.exports = userbyCoutries;


