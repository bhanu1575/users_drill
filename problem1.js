function peopleInterests(dataset,hooby){
    return Object.keys(dataset).filter((name)=>{
       return dataset[name].interests.reduce((acu,cv)=>{
            if(cv.includes(hooby)){
                acu = true;
            }
            return acu;
        },false);
    })
}

module.exports = peopleInterests;