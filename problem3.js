function designationMap(designation) {
  if (designation.includes("Senior")) {
    return 3;
  } else if (
    designation.includes("Developer") &&
    !designation.includes("Senior")
  ) {
    return 2;
  } else if (designation.includes("Intern")) {
    return 1;
  }
}

function sortByDesignation(dataset) {
  let newdata = Object.keys(dataset).reduce((acu, cv) => {
    acu.push({
      name: cv,
      designation: dataset[cv].desgination,
    });
    return acu;
  }, []);

  newdata.sort((a, b) => {
    return designationMap(a.designation) - designationMap(b.designation);
  });
  return newdata.reduce((acu, cv) => {
    acu[cv.name] = dataset[cv.name];
    return acu;
  }, {});
}

function sortByAge(dataset) {
  let dataArray = Object.keys(dataset).reduce((acu, cv) => {
    acu.push({
      name: cv,
      age: dataset[cv].age,
    });
    return acu;
  }, []);
  dataArray.sort((a, b) => {
    return a.age - b.age;
  });

  return dataArray.reduce((acu, cv) => {
    acu[cv.name] = dataset[cv.name];
    return acu;
  }, {});
}

module.exports = { sortByDesignation, sortByAge };
