function groupByLanguages(users){

    
    const languages = ['Python','Golang','Javascript']

   return languages.reduce((accumulator1,pl)=>{
        return Object.keys(users).reduce((accumulator2,nam)=>{
        if(!accumulator2[pl]){
            accumulator2[pl] = [];
        }
        if(users[nam].desgination.includes(pl)){
            accumulator2[pl].push(nam);
        }
        return accumulator2;
    },accumulator1)
},{})

}

module.exports = groupByLanguages;